#include <fstream>
#include <stdio.h>

//bf is brainfuck

int main(int argc, char *argv[]) {
	if(argc < 2) return 2; //error unknown file
	
	unsigned char cells [30000] ={0};//create bf cells 
	unsigned char *pointer = &cells[0];//create pointer for pf command < and >
	
	std::ifstream bfile(argv[1], std::ios::in | std::ios::binary);//open bf file on arguments
	
	char ch; //for sfe and processing command
	
	char processing[21]; //it is for processing function zerr(), add(), etc.
	
	while(!bfile.eof())
	{
		bfile.get(ch);
		
		
		if(ch == '#') bfile.ignore(1000, '\n');//# is comment, if comment ignore symbols to new line
		
		
		else if(ch == '>') //it is command for go left cell
		{
			if(pointer == &cells[29999]) pointer = &cells[0];//if pointer on end of cells return file on start of cells
			else pointer++;//else pointer go left
		}
		
		else if(ch=='<')//it is command for go right cell
		{
			if(pointer == &cells[0]) pointer = &cells[29999];//if pointer on start stay pointer on end
			else pointer--;//else pointer go right
		}
		
		else if(ch == '+')//it is plus one to current cell
		{
			*pointer++;
		}
		
		else if(ch == '-' )//it is plus one to current cell minus
		{
			*pointer--;
		}
		
		
		else if(ch == '.')//write in console value current cell 
		{
			putchar(*pointer);//putchar is c std function, it write symbol on console
		}
		
		else if(ch == ',')//get value on console
		{
			*pointer = getchar();//getchar get char value on console
		}
		
		
	}
	
	getchar();
	return 0;
}